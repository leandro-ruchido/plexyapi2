from sql_alchemy import banco

class ConsultaErrorModel(banco.Model):
    __tablename__ = 'tb_consultas_get_error_logs'

    id_consulta_error = banco.Column(banco.Integer, primary_key=True)
    placa = banco.Column(banco.String(100))
    chassi = banco.Column(banco.String(100))
    data_consulta = banco.Column(banco.DateTime)

    def __init__(self, placa, chassi, data_consulta):
        self.placa = placa
        self.chassi = chassi
        self.data_consulta = data_consulta

    def json(self):
        return {
            'id_consulta_error': self.id_consulta_error,
            'placa': self.placa,
            'chassi': self.chassi,
            'data_consulta': self.data_consulta
        }

    @classmethod
    def find_consulta_error(cls, placa):
        consulta_error = cls.query.filter_by(placa=placa).first()
        if consulta_error:
            return consulta_error
        return None

    def save_consulta_error(self):
        banco.session.add(self)
        banco.session.commit()

    def update_consulta_error(self, placa, chassi, data_consulta):
        self.placa = placa,
        self.chassi = chassi,
        self.data_consulta = data_consulta