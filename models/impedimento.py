from sql_alchemy import banco

class ImpedimentoModel(banco.Model):
    __tablename__ = 'tb_consultas_impedimentos'

    id_impedimento = banco.Column(banco.Integer, primary_key=True)
    impedimento = banco.Column(banco.String(300))
    placa = banco.Column(banco.String(50))
    id_consulta = banco.Column(banco.Integer, banco.ForeignKey('tb_consultas.id_consulta'))

    def __init__(self, impedimento, placa, id_consulta):
        self.impedimento = impedimento
        self.placa = placa
        self.id_consulta = id_consulta

    def json(self):
        return {
            'id_impedimento': self.id_impedimento,
            'impedimento': self.impedimento,
            'placa': self.placa,
            'id_consulta': self.id_consulta
        }

    @classmethod
    def find_impedimento(cls, impedimento, placa):
        impedimento = cls.query.filter_by(impedimento=impedimento, placa=placa).first()
        if impedimento:
            return impedimento
        return None

    def save_impedimento(self):
        banco.session.add(self)
        banco.session.commit()

    def update_impedimento(self, impedimento, placa, id_consulta):
        self.impedimento = impedimento,
        self.placa = placa,
        self.id_consulta = id_consulta