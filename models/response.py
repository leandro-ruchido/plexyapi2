class ResponseModel:

    def __init__(self, placa, renavam, placaAnterior, municipio, municipioAnterior, marca, anoFabricacao,
                 anoModelo, ipvaPago, parcelaIpva, seguroPago, parcelaSeguro, seguroAnteriorPago, taxaLicenciamentoPaga,
                 dataLicenciamento, situacaoLicenciamento, numeroAr, comunicado, mensagemPendencias,
                 impedimentosRestricoes, mensagemAutuacoes, autuacoes, mensagemMultas, multas):
        self.placa = placa
        self.renavam = renavam
        self.placaAnterior = placaAnterior
        self.municipio = municipio
        self.municipioAnterior = municipioAnterior
        self.marca = marca
        self.anoFabricacao = anoFabricacao
        self.anoModelo = anoModelo
        self.ipvaPago = ipvaPago
        self.parcelaIpva = parcelaIpva
        self.seguroPago = seguroPago
        self.parcelaSeguro = parcelaSeguro
        self.seguroAnteriorPago = seguroAnteriorPago
        self.taxaLicenciamentoPaga = taxaLicenciamentoPaga
        self.dataLicenciamento = dataLicenciamento
        self.situacaoLicenciamento = situacaoLicenciamento
        self.numeroAr = numeroAr
        self.comunicado = comunicado
        self.mensagemPendencias = mensagemPendencias
        self.impedimentosRestricoes = impedimentosRestricoes
        self.mensagemAutuacoes = mensagemAutuacoes
        self.autuacoes = autuacoes
        self.mensagemMultas = mensagemMultas
        self.multas = multas

    def json(self):
        return {
            'placa': self.placa,
            'renavam': self.renavam,
            'placaAnterior': self.placaAnterior,
            'municipio': self.municipio,
            'municipioAnterior': self.municipioAnterior,
            'marca': self.marca,
            'anoFabricacao': self.anoFabricacao,
            'anoModelo': self.anoModelo,
            'ipvaPago': self.ipvaPago,
            'parcelaIpva': self.parcelaIpva,
            'seguroPago': self.seguroPago,
            'parcelaSeguro': self.parcelaSeguro,
            'seguroAnteriorPago': self.seguroAnteriorPago,
            'taxaLicenciamentoPaga': self.taxaLicenciamentoPaga,
            'dataLicenciamento': self.dataLicenciamento,
            'situacaoLicenciamento': self.situacaoLicenciamento,
            'numeroAr': self.numeroAr,
            'comunicado': self.comunicado,
            'mensagemPendencias': self.mensagemPendencias,
            'impedimentosRestricoes': self.impedimentosRestricoes,
            'mensagemAutuacoes': self.mensagemAutuacoes,
            'autuacoes': self.autuacoes,
            'mensagemMultas': self.mensagemMultas,
            'multas': self.multas
        }
