class PlexyModel:
    def __init__(self, placa, chassi):
        self.placa = placa
        self.chassi = chassi

    def json(self):
        return {
            'placa': self.placa,
            'chassi': self.chassi
        }