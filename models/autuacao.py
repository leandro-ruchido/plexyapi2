from sql_alchemy import banco

class AutuacaoModel(banco.Model):
    __tablename__ = 'tb_consultas_autuacoes'

    id_autuacao = banco.Column(banco.Integer, primary_key=True)
    orgao = banco.Column(banco.String(100))
    descricaoSituacao = banco.Column(banco.String(100))
    marca = banco.Column(banco.String(100))
    codigo = banco.Column(banco.String(100))
    data = banco.Column(banco.String(20))
    hora = banco.Column(banco.String(20))
    descricaoInfracao = banco.Column(banco.String(200))
    endereco = banco.Column(banco.String(200))
    municipio = banco.Column(banco.String(100))
    dataInclusaoInfracao = banco.Column(banco.String(100))
    dataLimiteDefesa = banco.Column(banco.String(100))
    aitNotificacao = banco.Column(banco.String(100))
    numeroProcessamento = banco.Column(banco.String(100))
    id_consulta = banco.Column(banco.Integer, banco.ForeignKey('tb_consultas.id_consulta'))

    def __init__(self, orgao, descricaoSituacao, marca, codigo, data,
                 hora, descricaoInfracao, endereco, municipio,
                 dataInclusaoInfracao, dataLimiteDefesa,
                 aitNotificacao, numeroProcessamento, id_consulta):
        self.orgao = orgao
        self.descricaoSituacao = descricaoSituacao
        self.marca = marca
        self.codigo = codigo
        self.data = data
        self.hora = hora
        self.descricaoInfracao = descricaoInfracao
        self.endereco = endereco
        self.municipio = municipio
        self.dataInclusaoInfracao = dataInclusaoInfracao
        self.dataLimiteDefesa = dataLimiteDefesa
        self.aitNotificacao = aitNotificacao
        self.numeroProcessamento = numeroProcessamento
        self.id_consulta = id_consulta

    def json(self):
        return {
            'id_autuacao': self.id_autuacao,
            'orgao': self.orgao,
            'descricaoSituacao': self.descricaoSituacao,
            'marca': self.marca,
            'codigo': self.codigo,
            'data': self.data,
            'hora': self.hora,
            'descricaoInfracao': self.descricaoInfracao,
            'endereco': self.endereco,
            'municipio': self.municipio,
            'dataInclusaoInfracao': self.dataInclusaoInfracao,
            'dataLimiteDefesa': self.dataLimiteDefesa,
            'aitNotificacao': self.aitNotificacao,
            'numeroProcessamento': self.numeroProcessamento,
            'id_consulta': self.id_consulta
        }

    @classmethod
    def find_autuacao(cls, aitNotificacao):
        autuacao = cls.query.filter_by(aitNotificacao=aitNotificacao).first()
        if autuacao:
            return autuacao
        return None

    def save_autuacao(self):
        banco.session.add(self)
        banco.session.commit()

    def update_autuacao(self, orgao, descricaoSituacao, marca, codigo, data,
                 hora, descricaoInfracao, endereco, municipio,
                 dataInclusaoInfracao, dataLimiteDefesa,
                 aitNotificacao, numeroProcessamento, id_consulta):
        self.orgao = orgao
        self.descricaoSituacao = descricaoSituacao
        self.marca = marca
        self.codigo = codigo
        self.data = data
        self.hora = hora
        self.descricaoInfracao = descricaoInfracao
        self.endereco = endereco
        self.municipio = municipio
        self.dataInclusaoInfracao = dataInclusaoInfracao
        self.dataLimiteDefesa = dataLimiteDefesa
        self.aitNotificacao = aitNotificacao
        self.numeroProcessamento = numeroProcessamento
        self.id_consulta = id_consulta