from sql_alchemy import banco


class ConsultaModel(banco.Model):
    __tablename__ = 'tb_consultas'

    id_consulta = banco.Column(banco.Integer, primary_key=True)
    placa = banco.Column(banco.String(100))
    renavam = banco.Column(banco.String(100))
    placaAnterior = banco.Column(banco.String(50))
    municipio = banco.Column(banco.String(100))
    municipioAnterior = banco.Column(banco.String(100))
    marca = banco.Column(banco.String(200))
    anoFabricacao = banco.Column(banco.String(100))
    anoModelo = banco.Column(banco.String(100))
    ipvaPago = banco.Column(banco.String(100))
    parcelaIpva = banco.Column(banco.String(100))
    seguroPago = banco.Column(banco.String(100))
    parcelaSeguro = banco.Column(banco.String(100))
    seguroAnteriorPago = banco.Column(banco.String(100))
    taxaLicenciamentoPaga = banco.Column(banco.String(100))
    dataLicenciamento = banco.Column(banco.String(100))
    situacaoLicenciamento = banco.Column(banco.String(200))
    numeroAr = banco.Column(banco.String(100))
    comunicado = banco.Column(banco.String(100))
    mensagemPendencias = banco.Column(banco.String(200))
    impedimentos = banco.relationship('ImpedimentoModel')
    mensagemAutuacoes = banco.Column(banco.String(100))
    autuacoes = banco.relationship('AutuacaoModel')
    mensagemMultas = banco.Column(banco.String(100))
    multas = banco.relationship('MultaModel')
    ultima_consulta = banco.Column(banco.DateTime)

    def __init__(self, placa, renavam, placaAnterior, municipio, municipioAnterior, marca, anoFabricacao,
                 anoModelo, ipvaPago, parcelaIpva, seguroPago, parcelaSeguro, seguroAnteriorPago, taxaLicenciamentoPaga,
                 dataLicenciamento, situacaoLicenciamento, numeroAr, comunicado, mensagemPendencias, mensagemAutuacoes,
                 mensagemMultas, ultima_consulta):
        self.placa = placa
        self.renavam = renavam
        self.placaAnterior = placaAnterior
        self.municipio = municipio
        self.municipioAnterior = municipioAnterior
        self.marca = marca
        self.anoFabricacao = anoFabricacao
        self.anoModelo = anoModelo
        self.ipvaPago = ipvaPago
        self.parcelaIpva = parcelaIpva
        self.seguroPago = seguroPago
        self.parcelaSeguro = parcelaSeguro
        self.seguroAnteriorPago = seguroAnteriorPago
        self.taxaLicenciamentoPaga = taxaLicenciamentoPaga
        self.dataLicenciamento = dataLicenciamento
        self.situacaoLicenciamento = situacaoLicenciamento
        self.numeroAr = numeroAr
        self.comunicado = comunicado
        self.mensagemPendencias = mensagemPendencias
        self.mensagemAutuacoes = mensagemAutuacoes
        self.mensagemMultas = mensagemMultas
        self.ultima_consulta = ultima_consulta

    def json(self):
        return {
            'id_consulta': self.id_consulta,
            'placa': self.placa,
            'renavam': self.renavam,
            'placaAnterior': self.placaAnterior,
            'municipio': self.municipio,
            'municipioAnterior': self.municipioAnterior,
            'marca': self.marca,
            'anoFabricacao': self.anoFabricacao,
            'anoModelo': self.anoModelo,
            'ipvaPago': self.ipvaPago,
            'parcelaIpva': self.parcelaIpva,
            'seguroPago': self.seguroPago,
            'parcelaSeguro': self.parcelaSeguro,
            'seguroAnteriorPago': self.seguroAnteriorPago,
            'taxaLicenciamentoPaga': self.taxaLicenciamentoPaga,
            'dataLicenciamento': self.dataLicenciamento,
            'situacaoLicenciamento': self.situacaoLicenciamento,
            'numeroAr': self.numeroAr,
            'comunicado': self.comunicado,
            'mensagemPendencias': self.mensagemPendencias,
            'mensagemAutuacoes': self.mensagemAutuacoes,
            'mensagemMultas': self.mensagemMultas,
            'ultima_consulta': self.ultima_consulta
        }

    @classmethod
    def find_consulta(cls, placa):
        consulta = cls.query.filter_by(placa=placa).first()
        if consulta:
            return consulta
        return None

    def save_consulta(self):
        banco.session.add(self)
        banco.session.commit()

    def update_consulta(self, placa, renavam, placaAnterior, municipio, municipioAnterior, marca, anoFabricacao,
                 anoModelo, ipvaPago, parcelaIpva, seguroPago, parcelaSeguro, seguroAnteriorPago, taxaLicenciamentoPaga,
                 dataLicenciamento, situacaoLicenciamento, numeroAr, comunicado, mensagemPendencias,
                 mensagemAutuacoes, mensagemMultas, ultima_consulta):
        self.placa = placa
        self.renavam = renavam
        self.placaAnterior = placaAnterior
        self.municipio = municipio
        self.municipioAnterior = municipioAnterior
        self.marca = marca
        self.marca = marca
        self.anoFabricacao = anoFabricacao
        self.anoModelo = anoModelo
        self.ipvaPago = ipvaPago
        self.parcelaIpva = parcelaIpva
        self.seguroPago = seguroPago
        self.parcelaSeguro = parcelaSeguro
        self.seguroAnteriorPago = seguroAnteriorPago
        self.taxaLicenciamentoPaga = taxaLicenciamentoPaga
        self.dataLicenciamento = dataLicenciamento
        self.situacaoLicenciamento = situacaoLicenciamento
        self.numeroAr = numeroAr
        self.comunicado = comunicado
        self.mensagemPendencias = mensagemPendencias
        self.mensagemAutuacoes = mensagemAutuacoes
        self.mensagemMultas = mensagemMultas
        self.ultima_consulta = ultima_consulta