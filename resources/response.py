from flask_restful import Resource, reqparse
from datetime import datetime
from models.response import ResponseModel
from models.consulta import ConsultaModel
from models.autuacao import AutuacaoModel
from models.multa import MultaModel
from models.impedimento import ImpedimentoModel


class ResponseMG(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('placa', type=str)
    atributos.add_argument('renavam', type=str)
    atributos.add_argument('placaAnterior', type=str)
    atributos.add_argument('municipio', type=str)
    atributos.add_argument('municipioAnterior', type=str)
    atributos.add_argument('marca', type=str)
    atributos.add_argument('anoFabricacao', type=str)
    atributos.add_argument('anoModelo', type=str)
    atributos.add_argument('ipvaPago', type=str)
    atributos.add_argument('parcelaIpva', type=str)
    atributos.add_argument('seguroPago', type=str)
    atributos.add_argument('parcelaSeguro', type=str)
    atributos.add_argument('seguroAnteriorPago', type=str)
    atributos.add_argument('taxaLicenciamentoPaga', type=str)
    atributos.add_argument('dataLicenciamento', type=str)
    atributos.add_argument('situacaoLicenciamento', type=str)
    atributos.add_argument('numeroAr', type=str)
    atributos.add_argument('comunicado', type=str)
    atributos.add_argument('mensagemPendencias', type=str)
    atributos.add_argument('impedimentosRestricoes', action='append')
    atributos.add_argument('mensagemAutuacoes', type=str)
    atributos.add_argument('autuacoes', action='append', type=dict)
    atributos.add_argument('mensagemMultas', type=str)
    atributos.add_argument('multas', action='append', type=dict)

    def post(self):
        now = datetime.now()
        dados = ResponseMG.atributos.parse_args()
        response = ResponseModel(**dados)
        request_dados = response.json()
        consulta_existe = ConsultaModel.find_consulta(request_dados['placa'])

        try:
            if consulta_existe:
                consulta_existe.update_consulta(request_dados['placa'], request_dados['renavam'],
                                                request_dados['placaAnterior'], request_dados['municipio'],
                                                request_dados['municipioAnterior'], request_dados['marca'],
                                                request_dados['anoFabricacao'], request_dados['anoModelo'],
                                                request_dados['ipvaPago'], request_dados['parcelaIpva'],
                                                request_dados['seguroPago'],
                                                request_dados['parcelaSeguro'], request_dados['seguroAnteriorPago'],
                                                request_dados['taxaLicenciamentoPaga'], request_dados['dataLicenciamento'],
                                                request_dados['situacaoLicenciamento'], request_dados['numeroAr'],
                                                request_dados['comunicado'], request_dados['mensagemPendencias'],
                                                request_dados['mensagemAutuacoes'], request_dados['mensagemMultas'],
                                                now)
                consulta_existe.save_consulta()
                print("Consulta Atualizada")
            else:
                consulta = ConsultaModel(
                    request_dados['placa'],
                    request_dados['renavam'],
                    request_dados['placaAnterior'],
                    request_dados['municipio'],
                    request_dados['municipioAnterior'],
                    request_dados['marca'],
                    request_dados['anoFabricacao'],
                    request_dados['anoModelo'],
                    request_dados['ipvaPago'],
                    request_dados['parcelaIpva'],
                    request_dados['seguroPago'],
                    request_dados['parcelaSeguro'],
                    request_dados['seguroAnteriorPago'],
                    request_dados['taxaLicenciamentoPaga'],
                    request_dados['dataLicenciamento'],
                    request_dados['situacaoLicenciamento'],
                    request_dados['numeroAr'],
                    request_dados['comunicado'],
                    request_dados['mensagemPendencias'],
                    request_dados['mensagemAutuacoes'],
                    request_dados['mensagemMultas'],
                    now
                )
                ConsultaModel.save_consulta(consulta)
                print("Consulta Inserida")

            id_consulta = ConsultaModel.find_consulta(request_dados['placa'])

            for campo in request_dados:

                # #IMPEDIMENTOS
                if campo == 'impedimentosRestricoes':
                    impedimentos = request_dados[campo]
                    if impedimentos is not None:
                        for impedimento in impedimentos:
                            print(impedimento)
                            impe_exist = ImpedimentoModel.find_impedimento(impedimento, request_dados['placa'])
                            if impe_exist:
                                impe_exist.update_impedimento(impedimento, request_dados['placa'], id_consulta.id_consulta)
                                impe_exist.save_impedimento()
                                print("Impedimento atualizado")
                            else:
                                novo_impedimento = ImpedimentoModel(
                                    impedimento,
                                    request_dados['placa'],
                                    id_consulta.id_consulta
                                )
                                ImpedimentoModel.save_impedimento(novo_impedimento)
                                print("Impedimento Inserido")

                # #AUTUAÇÃO
                if campo == 'autuacoes':
                    autuacoes = request_dados[campo]
                    if autuacoes is not None:
                        for autuacao in autuacoes:
                            autu_exist = AutuacaoModel.find_autuacao(autuacao['aitNotificacao'])

                            if autu_exist:
                                autu_exist.update_autuacao(autuacao['orgao'],
                                                           autuacao['descricaoSituacao'], autuacao['marca'],
                                                           autuacao['codigo'], autuacao['data'],
                                                           autuacao['hora'], autuacao['descricaoInfracao'],
                                                           autuacao['endereco'], autuacao['municipio'],
                                                           autuacao['dataInclusaoInfracao'], autuacao['dataLimiteDefesa'],
                                                           autuacao['aitNotificacao'], autuacao['numeroProcessamento'],
                                                           id_consulta.id_consulta)
                                autu_exist.save_autuacao()
                                print("Autuação atualizada")
                            else:
                                nova_autuacao = AutuacaoModel(
                                    autuacao['orgao'],
                                    autuacao['descricaoSituacao'],
                                    autuacao['marca'],
                                    autuacao['codigo'],
                                    autuacao['data'],
                                    autuacao['hora'],
                                    autuacao['descricaoInfracao'],
                                    autuacao['endereco'],
                                    autuacao['municipio'],
                                    autuacao['dataInclusaoInfracao'],
                                    autuacao['dataLimiteDefesa'],
                                    autuacao['aitNotificacao'],
                                    autuacao['numeroProcessamento'],
                                    id_consulta.id_consulta
                                )
                                AutuacaoModel.save_autuacao(nova_autuacao)
                                print("Autuação Inserida")
                # #MULTA
                if campo == 'multas':
                    print(campo)
                    multas = request_dados[campo]
                    print(multas)
                    if multas is not None:
                        for multa in multas:
                            print(multa)
                            multa_exist = MultaModel.find_multa(multa['aitNotificacao'])
                            if multa_exist:
                                multa_exist.update_multa(multa['orgao'],
                                                         multa['descricaoSituacao'], multa['marca'],
                                                         multa['codigo'], multa['data'],
                                                         multa['hora'], multa['descricaoInfracao'],
                                                         multa['endereco'], multa['municipio'],
                                                         multa['dataInclusaoInfracao'], multa['dataLimiteRecurso'],
                                                         multa['aitNotificacao'], multa['numeroProcessamento'],
                                                         multa['valor'], id_consulta.id_consulta)
                                multa_exist.save_multa()
                                print("Multa atualizada")
                            else:
                                nova_multa = MultaModel(
                                    multa['orgao'],
                                    multa['descricaoSituacao'],
                                    multa['marca'],
                                    multa['codigo'],
                                    multa['data'],
                                    multa['hora'],
                                    multa['descricaoInfracao'],
                                    multa['endereco'],
                                    multa['municipio'],
                                    multa['dataInclusaoInfracao'],
                                    multa['dataLimiteRecurso'],
                                    multa['aitNotificacao'],
                                    multa['numeroProcessamento'],
                                    multa['valor'],
                                    id_consulta.id_consulta
                                )
                                MultaModel.save_multa(nova_multa)
                                print("Multa Inserida")
            return {'message': 'Data successfully received!'}, 200
        except:
            return {'message': 'An internal error ocurred trying to save.'}, 500