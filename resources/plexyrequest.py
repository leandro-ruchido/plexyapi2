from flask_restful import Resource, reqparse
from models.plexyrequest import PlexyModel
from models.consulta_error import ConsultaErrorModel
from datetime import datetime
import requests
import time

class PlexyRequest(Resource):
    atributos = reqparse.RequestParser()
    atributos.add_argument('placa', required=True, type=str, help="The field 'placa' is required")
    atributos.add_argument('chassi', required=True, type=str, help="The field 'chassi' is required")

    def post(self):
        try:
            dados = PlexyRequest.atributos.parse_args()
            plexy_objeto_mg = PlexyModel(**dados)
            request_dados_mg = plexy_objeto_mg.json()
            print(request_dados_mg)

            url_mg = 'https://api.plexi.com.br//api/maestro/detran-mg/veiculos/situacao-veiculo'
            api_key = '90vfG3mGDisIqKYEvqg69eb7i9qyJCATIj98SmIZzjmD9m5J8FDwH7gytuGsoSeCbwIssYY2RvWJBkRjzal0RSDIKPLft2DrLn1X'

            headers_mg = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + api_key
            }

            body = {
                'placa': request_dados_mg['placa'],
                'chassi': request_dados_mg['chassi']
            }

            resposta = requests.request('POST', url_mg, json=body, headers=headers_mg)

            # Coletou a request Id da requisição
            request_id = resposta.json()

            #GET
            headers_get = {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + api_key
            }

            response_url = 'https://api.plexi.com.br/api/maestro/result/' + request_id['requestId']

            resposta_plexi = requests.request('GET', response_url, headers=headers_get)

            while resposta_plexi.status_code == 202:
                time.sleep(5)
                resposta_plexi = requests.request('GET', response_url, headers=headers_get)
                print(resposta_plexi.status_code)
                print("Aguarde 5 segundos para uma nova verificação!")

            print(resposta_plexi.json())
            return {'message': 'Response received successfully!'}, 200
        except:
            dados = PlexyRequest.atributos.parse_args()
            plexy_objeto_mg = PlexyModel(**dados)
            request_dados_mg = plexy_objeto_mg.json()
            consulta_error_existe = ConsultaErrorModel.find_consulta_error(request_dados_mg['placa'])
            if consulta_error_existe:
                consulta_error_existe.update_consulta_error(request_dados_mg['placa'], request_dados_mg['chassi'], datetime.now())
                consulta_error_existe.save_consulta_error()
                return {'message': 'An internal error occurred when request!'}, 500
            novo_error = ConsultaErrorModel(
                request_dados_mg['placa'],
                request_dados_mg['chassi'],
                datetime.now()
            )
            ConsultaErrorModel.save_consulta_error(novo_error)
            return {'message': 'An internal error occurred when requested'}, 500