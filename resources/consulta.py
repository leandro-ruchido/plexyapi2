from flask_restful import Resource, reqparse
from models.consulta import ConsultaModel


class Consulta(Resource):
    def get(self, placa):
        consulta = ConsultaModel.find_consulta(placa)
        print(consulta)
        if consulta:
            return consulta.json(), 200
        return {'message': 'License plate not found.'}, 500