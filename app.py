from flask import Flask
from flask_restful import Api
from resources.response import ResponseMG
from resources.consulta import Consulta
from resources.plexyrequest import PlexyRequest

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:kAwiVrwDeC6R05kzxw7fwCUP26@3.135.176.198/somultas_homologacao'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api = Api(app)

api.add_resource(PlexyRequest, '/plexy/mg')
api.add_resource(ResponseMG, '/plexy/responsemg')
api.add_resource(Consulta, '/plexy/responsemg/<string:placa>')

if __name__ == '__main__':
    from sql_alchemy import banco
    banco.init_app(app)
    app.run(debug=True)